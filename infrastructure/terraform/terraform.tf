terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.23.0"
    }
  }
  backend "http" {  
  }
  
  provider "aws" {

  region = var.region

}
}

module "k8s-networking" {
  source = "./modules/k8s-networking"

}

# module "k8s-control-plane-ec2" {
#   source = "../modules/k8s-control-plane-ec2"

# }

# module "k8s-node-ec2" {
#   source = "../modules/k8s-node-ec2"
# }