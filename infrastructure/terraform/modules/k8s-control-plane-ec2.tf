data "aws_ami" "ubuntu" {
  most_recent = true
  filter = []
} 
resource "aws_instance" "k8s_control_plane" {
  ami = data.aws_ami.ubuntu
  instance_type = "t2.medium"
  tags = {
    Name = "K8s Control Plane"
  }
}

