resource "aws_vpc" "k8s_vpc" {
    cidr_block = "10.0.0.0/16"
    enable_dns_support = true
    enable_dns_hostnames = true
    tags = {
        "name": "chaosexperiments-vpc"
        "application": "kubernetes"
    }
}
resource "aws_subnet" "k8s_public_subnet" {
    vpc_id = var.aws_vpc.k8s_vpc.id
    cidr_block = "10.0.1.0/24"
    availability_zone = var.az
    map_public_ip_on_launch = true
    tags = {
        "name": "chaosexperiments-subnet"
        "application": "kubernetes"
    }
}
resource "aws_internet_gateway" "k8s_igw" {
    vpc_id = var.aws_vpc.k8s_vpc.id
    tags = {
        "name": "chaosexperiments-igw"
        "application": "kubernetes"
    }
}
resource "aws_route_table" "k8s_rt" {
    vpc_id = var.aws_vpc.k8s_vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.k8s_igw.id
    }
    tags = {
        "name": "chaosexperiments-rt"
        "application": "kubernetes"
    }
}
resource "aws_route_table_association" "k8s_rt_subnet_assoc" {
    subnet_id = var.aws_subnet.k8s_public_subnet.id
    route_table_id = var.aws_route_table.k8s_rt.id
}
resource "aws_security_group" "k8s_sg" {
    vpc_id = var.aws_vpc.k8s_vpc.id
    name        = "allow_tls"
  description = "Allow TLS inbound traffic"

  ingress {
    description      = "All Traffic from subnet"
    from_port        = -1
    to_port          = -1
    protocol         = "-1"
    cidr_blocks      = [aws_subnet.k8s_public_subnet.cidr_block]
  }

  ingress {
    description      = "Ping Traffic from subnet"
    from_port        = -1
    to_port          = -1
    protocol         = "1"
    cidr_blocks      = [aws_subnet.k8s_public_subnet.cidr_block]
  }


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}