variable "region" {
    default = "us-east-1"
}
variable "az" {
    default = "us-east-1a"
}
variable "nodeCount" {
    default = 1
}

